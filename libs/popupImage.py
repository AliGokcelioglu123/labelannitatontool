import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
import config as cfg

class PopupImage(QDialog):
    def __init__(self, itemPos, pixmap):
        super().__init__()
        self.title = "PyQt5 Adding Image To Label"
        self.top = 200
        self.left = 500
        self.width = 400
        self.height = 300
        self.InitWindow(itemPos, pixmap)

    def InitWindow(self, itemPos, pixmap):
        scale = cfg.roiHoverWidth / pixmap.width()
        h = pixmap.height() * scale
        self.image = QLabel("Shape")
        self.image.setPixmap(pixmap)
        self.image.setScaledContents(True)
        self.image.setWindowFlags(Qt.FramelessWindowHint)
        self.image.setAttribute(Qt.WA_TranslucentBackground, True)
        self.image.setGeometry(itemPos.x() - cfg.roiHoverWidth, QDesktopWidget().height() / 2  - h, cfg.roiHoverWidth, h)
        self.image.show()

    def show(self):
        self.image.show()

    def close(self):
        self.image.close()

if __name__ == "__main__":
    App = QApplication(sys.argv)
    popupimage = popupimage()
    sys.exit(App.exec())

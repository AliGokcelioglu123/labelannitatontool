from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *


#from PyQt4.QtOpenGL import *

from libs.shape import Shape
from libs.utils import distance
from libs.labelFile import LabelFile
from libs.templateMatch import getMatch
from .canvas import Canvas

import config as cfg
import cv2

CURSOR_DEFAULT = Qt.ArrowCursor
CURSOR_POINT = Qt.PointingHandCursor
CURSOR_DRAW = Qt.CrossCursor
CURSOR_MOVE = Qt.ClosedHandCursor
CURSOR_GRAB = Qt.OpenHandCursor



class SubCanvas(Canvas, QDialog):
    def __init__(self, image, shapes):
        super(SubCanvas, self).__init__()

        self.image  = image
        self.shapes = shapes


    def load(self):
        self.pixmap = QPixmap.fromImage(self.image)
        self.repaint()

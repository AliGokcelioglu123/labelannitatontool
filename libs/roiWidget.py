import sys
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

class RoiWidget(QWidget):
    shift = pyqtSignal(tuple)

    def __init__(self):
        super(RoiWidget, self).__init__(parent=None)

        text = QLabel("Roi")
        self.x = QLineEdit()
        self.x.setText("X")
        self.x.returnPressed.connect(self.shiftRoi)

        self.y = QLineEdit()
        self.y.setText("Y")
        self.y.returnPressed.connect(self.shiftRoi)

        self.w = QLineEdit()
        self.w.setText("W")
        self.w.returnPressed.connect(self.shiftRoi)

        self.h = QLineEdit()
        self.h.setText("H")
        self.h.returnPressed.connect(self.shiftRoi)

        fourDigitInt = QRegExp("\\d{1,4}")
        isInt = QRegExpValidator(fourDigitInt, self)

        self.x.setValidator(isInt)
        self.y.setValidator(isInt)
        self.w.setValidator(isInt)
        self.h.setValidator(isInt)

        vbox = QVBoxLayout()
        hbox = QHBoxLayout()
        hbox.addWidget(text)
        hbox.addWidget(self.x)
        hbox.addWidget(self.y)
        hbox.addWidget(self.w)
        hbox.addWidget(self.h)
        vbox.addLayout(hbox)
        self.area = QLabel("Area:")
        self.area.setAlignment(Qt.AlignTop | Qt.AlignHCenter)
        vbox.addWidget(self.area)
        self.setLayout(vbox)
        self.show()

    def shiftRoi(self):
        x = int(self.x.text())
        y = int(self.y.text())
        w = int(self.w.text())
        h = int(self.h.text())
        return self.shift.emit((x, y, w, h))

    def reset(self):
        self.x.setText("X")
        self.y.setText("Y")
        self.w.setText("W")
        self.h.setText("H")
        self.area.setText("Area: {}px".format("0"))

    def setRoi(self, x, y, w, h):
        self.x.setText(str(x))
        self.y.setText(str(y))
        self.w.setText(str(w))
        self.h.setText(str(h))
        self.area.setText("Area: {}px".format(w*h))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = RoiWidget()
    sys.exit(app.exec_())

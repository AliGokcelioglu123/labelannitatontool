import numpy as np
import matplotlib.pyplot as plt

from skimage import data
from skimage import io
import cv2
from skimage.feature import match_template

from PIL import Image

def load_image( infilename ) :
    img = Image.open( infilename )
    img.load()
    data = np.asarray( img, dtype="int32" )
    return img


def getMatch(template, parent):

    result = match_template(parent, template)
    ij = np.unravel_index(np.argmax(result), result.shape)
    x, y = ij[::-1]

    # highlight matched region
    h, w = template.shape

    return x, y, w, h

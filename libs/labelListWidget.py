import sys
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *


class LabelListWidget(QListWidget, QListView):
    popDownImage = pyqtSignal(bool)

    def __init__(self):
        super(LabelListWidget, self).__init__(parent=None)
        self.setMouseTracking(True)


    def leaveEvent(self, event):
        self.popDownImage.emit(False)

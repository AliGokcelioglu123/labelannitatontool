from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

#suan bos

#these two need to be the same keys, one key one modifier
multipleSelectionModifier = Qt.ShiftModifier
multipleSelectionKey = Qt.Key_Shift


roiHoverWidth = 200

keepShapes  = 'Ctrl+D'
selectAll   = 'Ctrl+A'
autoCopy    = "Ctrl+V"
copyByClick = "Ctrl+C"

epsilon = 0.10
cornerEpsilon = 2.0

autoLabels = {"cpu" : 2, "latch" : 17}
